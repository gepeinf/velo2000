Flex
Quellen
Einstellungen in GIT: https://padlet.com/stbaeumer/Programmierumgebung
Musterlösung: https://bitbucket.org/stbaeumer/velo2000-loesung/
Erklärvideo: https://bk-borken.lms.schulon.org/mod/url/view.php?id=6359
Die Gestaltungsmöglichkeiten von flex in CSS
In HTML kann man eine unsortierte Liste mit dem ul-Tag erstellen. Jedes Element der Liste wird mit li dargestellt.

<!--Unsortierte Liste-->
<ul>
<li>Listenpunkt 1</li>
<li>Listenpunkt 2</li>
</ul>
Das passiert ... und bewirkt

ul{
    list-style-type: none;
    margin: 0;
    padding: 0;
    border:1px dotted red;
    display:flex;
    flex-direction: row;    
    flex-wrap: wrap-reverse;
    justify-content: space-around;
    height: 200px;
    align-items: center;
}
Text

ul {

}



